#Project 6
#10/8/15
#Brady Russ


import pet


myPet = pet.Pet("Fido", "Dog", 5)
print("The name of my pet is", (myPet.get_name()))
print("The animal type of my pet is", (myPet.get_animal_type()))
print("The age of my pet is", str(myPet.get_age()))
new_name = input("Enter a new name for your pet:")
myPet.set_name(new_name)
print("The new name of my pet is", myPet.get_name())
new_age = input("Enter a new age for your pet:")
new_type = input("Enter a new animal type for your pet:")

