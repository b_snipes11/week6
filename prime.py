#is_prime Module
#Week 6 Folder
#Brady Russ
#10/5/15

def is_prime(num):

    if num < 2:
        print(num, "is not a valid integer. Please try again")
        return False
    else:
        #check all numbers 2 to one less than the number
        #when one of these goes into the number, return false
        for x in range(2, num):
            if(num % x == 0):
                return False

    return True

